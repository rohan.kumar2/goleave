const functions = require('firebase-functions');
const admin = require('firebase-admin');
// Fetch the service account key JSON file contents
const serviceAccount = require('./AdminSDK.json');

// Initialize the app with a service account, granting admin privileges
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://goleave-1a818.firebaseio.com',
});

exports.createNewUserAccount = functions.https.onCall((data, context) => {
  if (context.auth.token.admin) {
    return admin.auth().createUser(data)
      .then((user) => {
        // See the UserRecord reference doc for the contents of userRecord.
        console.log('Successfully created new user:', user.uid);
        console.log('Department', data.dept);
        // eslint-disable-next-line no-use-before-define
        assignRole(user.uid, data.dept);
        return { user };
      // grantRole(userRecord.email,request.body.userdata.department);
      }).catch((error) => {
        console.log(error);
        return {
          error,
        };
      });
  }
  return {
    error: 'Unauthorized Request!',
  };
});

exports.helloWorld = functions.https.onCall((data, context) => {
  if (context.auth.token.admin) {
    return {
      result: `Hello from ${data}`,
    };
  }
  return {
    error: 'Unauthorized access!',
  };
});

// eslint-disable-next-line consistent-return
exports.addRole = functions.https.onCall((data, context) => {
  if (context.auth.token.admin) {
    const { uid, dept } = data.claim;
    console.log('here is UID', uid);
    console.log('here is dept', dept);
    // eslint-disable-next-line no-use-before-define
    assignRole(uid, dept);
    console.log('Claims added!');
  } else {
    return { error: 'Unauthorized Request!' };
  }
});

async function assignRole(uid, department) {
  const user = await admin.auth().getUser(uid);
  console.log('this is user: ', user);
  if (user.customClaims && (user.customClaims.admin
     || user.customClaims.manager || user.customClaims.employee)) {
    return;
  }
  if (department === 'Human Resource' || department === 'Management') {
    console.log('here is admin custom claim');
    admin.auth().setCustomUserClaims(user.uid, {
      admin: true,
    });
    // return admin.auth().setCustomUserClaims(user.uid, {
    //   admin: true,
    // });
  } else if (department === 'Manager') {
    console.log('here is Manager custom claim');
    admin.auth().setCustomUserClaims(user.uid, {
      manager: true,
    });
  } else {
    console.log('here is employee custom claim');
    admin.auth().setCustomUserClaims(user.uid, {
      employee: true,
    });
  }
}
// async function getclaims(email) {
//   const user = await admin.auth().getUserByEmail(email);
//   console.log('this is user: ', user);
//   if (user.customClaims && (user.customClaims.admin)) {
//     console.log('yes this is true that is admin');
//     return true;
//   }
//   else if (user.customClaims && (user.customClaims.manager)) {
//     console.log('yes this is true that is manager');
//     return true;
//   }
//   else if (user.customClaims && (user.customClaims.employee)) {
//     console.log('yes this is true that is employee');
//     return true;
//   }
//   return false;
// }
