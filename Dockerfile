FROM node:lts-alpine

ARG UID
ARG GID
ARG USER=node
ARG GROUP=node
RUN test -n "$UID"
RUN test -n "$GID"

RUN apk update \
    && apk add shadow musl-utils \
    && wget -O /usr/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 \
    && chmod +x /usr/bin/dumb-init \
    && if [ "`getent group $GID | cut -d: -f3`" != "`getent group $GROUP| cut -d: -f3`" ]; then \
        groupmod -g ${GID} ${GROUP}; \
      fi \
    && if [ "`getent passwd $UID | cut -d: -f3`" != "`getent passwd $USER | cut -d: -f3`" ]; then \
        usermod -g ${GID} -u ${UID} ${USER}; \
      fi \
    && apk add openjdk8-jre

COPY --chown=$USER:$GROUP . /app

ARG VUE_CLI_SERVICE_PORT=8080
ARG FIREBASE_LOGIN_PORT=9005
ARG EMULATOR_HOSTING_PORT=5000
ARG EMULATOR_FUNCTIONS_PORT=5001
ARG EMULATOR_FIRESTORE_PORT=8081
ARG EMULATOR_UI_PORT=9090
WORKDIR /app
USER $USER
EXPOSE ${VUE_CLI_SERVICE_PORT} ${FIREBASE_LOGIN_PORT} ${EMULATOR_FUNCTIONS_PORT} ${EMULATOR_HOSTING_PORT} ${EMULATOR_UI_PORT}

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/bin/sh", "-c", "npm install && npm run serve"]
