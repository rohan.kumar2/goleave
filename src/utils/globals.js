const ModelClasses = {};

function registerClass(clsName, cls) {
  ModelClasses[clsName] = cls;
}

function getClassFromName(clsName) {
  return ModelClasses[clsName];
}

export {
  registerClass,
  getClassFromName,
};
