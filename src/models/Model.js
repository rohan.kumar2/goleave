/* eslint-disable no-restricted-syntax */
import * as firebase from 'firebase/app';
import 'firebase/firestore';
// import * as admin from 'firebase-admin';
// import axios from 'axios';
import { getClassFromName } from '@/utils/globals';
import Logger from '@/logger/index';
import store from '@/store/index';
import ValidationError from '../exceptions/ValidationError';
import InvalidPathError from '../exceptions/InvalidPathError';
import IllegalArgumentError from '../exceptions/IllegalArgumentError';

export default class BaseModel {
  constructor(args) {
    console.log('hello this is model classs');
    const cls = getClassFromName(this.getClassName());
    // Set up database reference in this object
    this.db = firebase.firestore();
    console.log(`Initializing class ${this.getClassName()}`);
    // Logger.debug(`Initializing class ${this.getClassName()}`);
    for (const fieldKey of Object.keys(cls.fields)) {
      if (args && args[fieldKey]) {
        this[fieldKey] = args[fieldKey];
      } else {
        // TODO: Set the default value to '' or null based on whether
        // the type is String or not
        this[fieldKey] = cls.fields[fieldKey].default;
      }
    }

    if (cls.subCollections) {
      for (const fieldKey of Object.keys(cls.subCollections)) {
        console.log('Yes it exits sub collection on employee ****');
        // eslint-disable-next-line new-cap
        this[fieldKey] = new cls.subCollections[fieldKey].type({
          className: cls.subCollections[fieldKey].subType.getClassName(),
        });
      }
    }
    // If nested is set to true then all nested subCollections are
    // fetched, updated, created as well as deleted.
    // Default is true
    if (args && args.nested) {
      this.nested = args.nested;
    } else {
      this.nested = true;
    }

    // This is set to true as soon as subCollections are initialized
    this.subCollectionInitialized = false;
  }

  // eslint-disable-next-line class-methods-use-this
  get path() {
    throw new Error('Subclass must override this method');
  }

  // eslint-disable-next-line class-methods-use-this
  initializeSubCollection(subCollectionField) {
    throw new Error('Subclass must override this method', subCollectionField);
  }

  validate() {
    const cls = getClassFromName(this.getClassName());
    for (const fieldKey of Object.keys(cls.fields)) {
      const field = cls.fields[fieldKey];
      if (field.required === true && this[fieldKey] === null) {
        throw new ValidationError(`field ${fieldKey} is required`);
      }
    }
  }

  addSubCollection(val, clazz) {
    const cls = getClassFromName(this.getClassName());

    if (!clazz) {
      throw new Error('Class type is required to add object to collection');
    }

    if (cls.subCollections) {
      console.log('you intialize your all fields');
      for (const fieldKey of Object.keys(cls.subCollections)) {
        if (cls.subCollections[fieldKey].subType === clazz) {
          if (!(val instanceof clazz)) {
            throw new Error(`${clazz.getClassName()} is not of subType:
              ${cls.subCollections[fieldKey].subType.getClassName()}`);
          }
          this[fieldKey].push(val);
        }
      }
    }
  }

  toJSON(proto) {
    const jsoned = {};
    const toConvert = proto || this;
    const cls = getClassFromName(this.getClassName());
    // console.log(this.getClassName(), Object.keys(cls.fields));
    Object.getOwnPropertyNames(toConvert).forEach((prop) => {
      const val = toConvert[prop];
      // don't include those
      // if (prop === 'toJSON' || prop === 'constructor') {
      //   return;
      // }
      // if (typeof val === 'function') {
      //   jsoned[prop] = val.bind(jsoned);
      //   return;
      // }
      if (Object.keys(cls.fields).includes(prop) && prop !== 'id') {
        jsoned[prop] = val;
      }
    });

    // const inherited = Object.getPrototypeOf(toConvert);
    // if (inherited !== null) {
    //   Object.keys(this.toJSON(inherited)).forEach((key) => {
    //     if (!!jsoned[key] || key === 'constructor' || key === 'toJSON') {
    //       return;
    //     }

    //     if (typeof inherited[key] === 'function') {
    //       jsoned[key] = inherited[key].bind(jsoned);
    //       return;
    //     }
    //     jsoned[key] = inherited[key];
    //   });
    // }
    return jsoned;
  }

  checkPath() {
    if (!this.path) {
      throw new InvalidPathError(
        'Cannot fetch object without valid path to collection',
      );
    }
  }

  checkId() {
    if (!this.id) {
      throw new IllegalArgumentError('Cannot update/delete object with no id');
    }
  }

  fetch() {
    // Check for path before making any firebase call
    this.checkPath();
    console.log('fetching in model class', this.id);
    console.log('path', this.path);

    const cls = getClassFromName(this.getClassName());
    // Logger.debug('fetch: ', this.toJSON());
    console.log('fetch: ', this.toJSON());
    return this.db
      .collection(`${this.path}`)
      .doc(this.id)
      .get()
      .then((doc) => {
        console.log('this is doc', doc.data());
        Object.keys(cls.fields).forEach((field) => {
          if (field !== 'id') {
            this[field] = doc.data()[field];
            console.log(`firebase: ${field}: `, this[field]);
          }
        });
      })
      .then(() => {
        const arr = [];
        if (this.nested && cls.subCollections) {
          // If subCollections have not been initialized, initialize
          // them now
          if (!this.subCollectionInitialized) {
            for (const fieldKey of Object.keys(cls.subCollections)) {
              this.initializeSubCollection(fieldKey);
            }
            this.subCollectionInitialized = true;
          }

          // Now fetch the nested subcollections
          // eslint-disable-next-line no-restricted-syntax
          for (const fieldKey of Object.keys(cls.subCollections)) {
            console.log('this.fieldKey: ', fieldKey, this[fieldKey]);
            arr.push(this[fieldKey].fetch());
          }
        }
        return Promise.all(arr).then(() => {
          console.log('All promises resolved. Returning true');
          return true;
        });
      });
  }

  create() {
    // Logger.debug('create: ', this.toJSON());
    console.log('create: ', this.toJSON());
    // Check for path before making any firebase call
    console.log('this is the database path', this.path);
    this.checkPath();

    // Validate the data before actual firebase call
    this.validate();
    if (this.docPath) {
      return this.db.collection(`${this.path}`)
        .doc(`${this.docPath}`)
        .set(this.toJSON())
        .then(() => {
          console.log(`Created note with id: ${this.docPath}`);
          this.id = this.docPath;
          const cls = getClassFromName(this.getClassName());
          if (this.nested && cls.subCollections) {
            console.log('*****this is also intiazlization by collection*****');
            for (const fieldKey of Object.keys(cls.subCollections)) {
              const subCollection = this[fieldKey].get();
              console.log('*****tcreate]*****', fieldKey, subCollection.length);
              const { subType } = cls.subCollections[fieldKey];
              if (subCollection.length > 0) {
                subCollection.forEach((val) => {
                // Call method in subclass to initialize each object
                // inside the subCollection array before it can be
                // created
                  this.initializeSubcollectionObject(val, subType);
                  val.create();
                });
              }
            }
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }
    return this.db.collection(`${this.path}`)
      .add(this.toJSON())
      .then((docRef) => {
        // Logger.debug(docRef);
        console.log(docRef);
        // Logger.info(`Created note with id: ${docRef.id}`);
        console.log(`Created note with id: ${docRef.id}`);
        this.id = docRef.id;

        // If there are any objects in nested collections, serialize
        // them too
        const cls = getClassFromName(this.getClassName());
        if (this.nested && cls.subCollections) {
          console.log('*****this is also intiazlization by collection*****');
          for (const fieldKey of Object.keys(cls.subCollections)) {
            const subCollection = this[fieldKey].get();
            console.log('*****tcreate]*****', fieldKey, subCollection.length);
            const { subType } = cls.subCollections[fieldKey];
            if (subCollection.length > 0) {
              subCollection.forEach((val) => {
                // Call method in subclass to initialize each object
                // inside the subCollection array before it can be
                // created
                this.initializeSubcollectionObject(val, subType);
                val.create();
              });
            }
          }
        }
      })
      .catch((error) => {
        console.log(`Could not create note
          ${JSON.stringify(this.toJSON(), null, 2)}`, error);
      })
      .finally(() => {
      });
  }

  update() {
    // Logger.debug('update: ', this.toJSON());
    console.log('update: ', this.toJSON());
    // Check for path before making any firebase call
    this.checkPath();

    // Check for a valid id before making any firebase call
    this.checkId();

    this.validate();
    if (!this.id) {
      throw new Error('Cannot update object with no id');
    }

    return this.db
      .collection(`${this.path}`)
      .doc(this.id)
      .update(this.toJSON())
      .then(() => {
        // Logger.info(`Updated doc with id ${this.id}`);
        console.log(`Updated doc with id ${this.id}`);
      })
      .catch((error) => {
        Logger.error(`Caught error ${error}`);
      })
      .finally(() => {
        // TODO: Do something before returning
      });
  }

  delete() {
    // Check for path before making any firebase call
    this.checkPath();
    console.log('this is delete model function', this.id);
    // Check for a valid id before making any firebase call
    this.checkId();
    return this.db
      .collection(`${this.path}`)
      .doc(this.id)
      .delete()
      .then(() => {
        // Logger.info(`Deleted note with id ${this.id}`);
        console.log(`Deleted note with id ${this.id}`);
        // TODO: Delete nested collections if any
      })
      .catch((error) => {
        // Logger.error(`Error ${error} while deleting object with id: ${this.id}`);
        console.log(`Error ${error} while deleting object with id: ${this.id}`);
      })
      .finally(() => {
      });
  }

  getJSONObject() {
    return this.toJSON();
  }

  async createnewuser() {
    console.log('create: ', this.toJSON());
    // Check for path before making any firebase call
    this.checkPath();

    // Validate the data before actual firebase call
    this.validate();

    await this.createAccount()
      .then((id) => {
        this.id = id;
      });

    return firebase.firestore()
      .collection(`${this.path}`)
      .doc(this.id)
      .set(this.toJSON())
      .then((docRef) => {
        // Logger.debug(docRef);
        console.log('**** this is Doc ******', docRef);
        // // Logger.info(`Created note with id: ${docRef.id}`);
        // console.log(`Created note with id: ${docRef.id}`);

        const cls = getClassFromName(this.getClassName());
        if (this.nested && cls.subCollections) {
          console.log('***You enter in nested collection ****');
          for (const fieldKey of Object.keys(cls.subCollections)) {
            const subCollection = this[fieldKey].get();
            const { subType } = cls.subCollections[fieldKey];
            console.log('***this is loop ****', subCollection.length, fieldKey, cls.subCollection);
            if (subCollection.length > 0) {
              subCollection.forEach((val) => {
                // Call method in subclass to initialize each object
                // inside the subCollection array before it can be
                // created
                console.log('***value inialize ****', val);
                this.initializeSubcollectionObject(val, subType);
                val.create();
              });
            }
          }
        }
      })
      .catch((error) => {
        console.log(`Could not create note
        ${JSON.stringify(this.toJSON(), null, 2)}`, error);
        store.dispatch('setSnackbarText', error.message);
        store.dispatch('setShowSnackbar', true);
        setTimeout(() => {
          this.$store.dispatch('setShowSnackbar', false);
        }, 4000);
      })
      .finally(() => {
      });
  }
}
