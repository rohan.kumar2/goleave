import Vue from 'vue';
import Model from '@/models/Model';
import { registerClass } from '@/utils/globals';

export default class Leave extends Model {
  static collectionName= 'totalleaves';

  static fields = {
    id: {
      type: String,
      default: null,
      required: false,
    },
    casualleave: {
      type: Number,
      default: 12,
      required: true,
    },
    medicalleave: {
      type: Number,
      default: 8,
      required: true,
    },
    dateCreated: {
      type: String,
      default: null,
      required: false,
    },
    dateUpdated: {
      type: String,
      default: null,
      required: false,
    },
  };

  constructor(args) {
    console.log('This is leave type model class');
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }

    if (args && args.noteid) {
      this.noteid = args.noteid;
    }
  }

  static getClassName() { return 'Leave'; }

  // eslint-disable-next-line class-methods-use-this
  getClassName() { return Leave.getClassName(); }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = `employee/${this.noteid}/${Leave.collectionName}`;
    }
    return collectionPath;
  }

  validate() {
    if (!this.uid) {
      throw new Error(
        'uid is required to fetch, create, delete or update note',
      );
    }
    super.validate();
  }

  create() {
    this.dateCreated = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    return super.create();
  }

  update() {
    this.dateUpdated = Vue.moment.utc().format('x');
    return super.update();
  }
}

registerClass(Leave.getClassName(), Leave);
