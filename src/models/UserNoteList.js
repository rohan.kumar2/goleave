import ListModel from '@/models/ListModel';
import Note from '@/models/Note';
// import Logger from '../logger';

export default class UserNoteList extends ListModel {
  constructor(args) {
    super(args);
    if (!args.uid) {
      throw new Error('uid is required for fetching Notes for a User');
    }
    console.log('hello this is usernotelist class');
    this.uid = args.uid;
  }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = 'leavetype';
    }
    return collectionPath;
  }

  /**
   * Factory method to create a new object from Object passed in `doc`
   * The data passed is from Firebase and it creates a Note object
   * @param {Object} doc
   */
  create(doc) {
    console.log(`Creating new note with id: ${doc.id} from firebase doc`);
    return new Note({ uid: this.uid, id: doc.id, ...doc.data() });
  }
}
