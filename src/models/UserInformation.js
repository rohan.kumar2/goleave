import Vue from 'vue';
import Model from '@/models/Model';
import { registerClass, getClassFromName } from '@/utils/globals';

export default class UserInformation extends Model {
  static collectionName= 'employee';

  static fields = {
    id: {
      type: String,
      default: null,
      required: false,
    },
    employeecode: {
      type: String,
      default: '',
      required: false,
    },
    gender: {
      type: String,
      default: '',
      required: true,
    },
    department: {
      type: String,
      default: '',
      required: false,
    },
    DOB: {
      type: String,
      default: '',
      required: true,
    },
    firstname: {
      type: String,
      default: '',
      required: true,
    },
    lastname: {
      type: String,
      default: '',
      required: true,
    },
    address: {
      type: String,
      default: '',
      required: true,
    },
    city: {
      type: String,
      default: '',
      required: true,
    },
    country: {
      type: String,
      default: '',
      required: true,
    },
    mobilenumber: {
      type: String,
      default: '',
      required: true,
    },
    email: {
      type: String,
      default: '',
      required: false,
    },
    dateCreated: {
      type: String,
      default: null,
      required: false,
    },
    dateUpdated: {
      type: String,
      default: null,
      required: false,
    },
  };

  constructor(args) {
    console.log('hello this is employee note list');
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }
  }

  static getClassName() { return 'Employee'; }

  initializeSubCollection(subCollectionField) {
    const cls = getClassFromName(this.getClassName());
    console.log(`this.uid: ${this.uid}, this.id: ${this.id}`);
    // eslint-disable-next-line new-cap
    this[subCollectionField] = new cls.subCollections[subCollectionField].type({
      className: cls.subCollections[subCollectionField].subType.getClassName(),
      uid: this.uid,
      noteid: this.id,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  getClassName() { return UserInformation.getClassName(); }

  get docPath() {
    return this.uid;
  }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = 'employee';
    }
    return collectionPath;
  }

  validate() {
    if (!this.uid) {
      throw new Error(
        'uid is required to fetch, create, delete or update note',
      );
    }
    super.validate();
  }

  create() {
    this.dateCreated = Vue.moment.utc().format('x');
    return super.create();
  }

  update() {
    this.dateUpdated = Vue.moment.utc().format('x');
    return super.update();
  }
}

registerClass(UserInformation.getClassName(), UserInformation);
