// import Vue from 'vue';
import Model from '@/models/Model';
import { registerClass } from '@/utils/globals';

export default class ApplyLeave extends Model {
  static collectionName= 'leaves';

  static fields = {
    id: {
      type: String,
      default: null,
      required: false,
    },
    name: {
      type: String,
      default: '',
      required: false,
    },
    leavetype: {
      type: String,
      default: '',
      required: true,
    },
    to: {
      type: String,
      default: '',
      required: true,
    },
    from: {
      type: String,
      default: '',
      required: true,
    },
    description: {
      type: String,
      default: '',
      required: true,
    },
    status: {
      type: String,
      default: 'Awaiting Approval',
      required: false,
    },
    // remark: {
    //   type: String,
    //   default: '',
    //   required: false,
    // },
    dateCreated: {
      type: String,
      default: null,
      required: false,
    },
    dateUpdated: {
      type: String,
      default: null,
      required: false,
    },
    approvalDate: {
      type: String,
      default: null,
      required: false,
    },
  };

  constructor(args) {
    console.log('hello this is apply leave list');
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }
  }

  static getClassName() { return 'ApplyLeave'; }

  // eslint-disable-next-line class-methods-use-this
  getClassName() { return ApplyLeave.getClassName(); }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = `employee/${this.uid}/${ApplyLeave.collectionName}`;
    }
    return collectionPath;
  }

  validate() {
    if (!this.uid) {
      throw new Error(
        'uid is required to fetch, create, delete or update note',
      );
    }
    super.validate();
  }

  create() {
    this.dateCreated = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    return super.create();
  }

  update() {
    // Set the dateCreated before calling superclass method to create
    this.dateCreated = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    return super.update();
  }

  updateStatus() {
    this.approvalDate = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    return super.update();
  }
}

registerClass(ApplyLeave.getClassName(), ApplyLeave);
