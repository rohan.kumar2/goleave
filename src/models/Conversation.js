// import Vue from 'vue';
import Model from '@/models/Model';
import { registerClass } from '@/utils/globals';

export default class Conversation extends Model {
  static collectionName= 'conversation';

  static fields = {
    id: {
      type: String,
      default: null,
      required: false,
    },
    remark: {
      type: String,
      default: '',
      required: false,
    },
    sentAt: {
      type: String,
      default: null,
      required: false,
    },
  };

  constructor(args) {
    console.log('This is conversation type model class');
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }

    if (args && args.noteid) {
      this.noteid = args.noteid;
    }
    if (args && args.id) {
      this.id = args.id;
    }
  }

  static getClassName() { return 'Conversation'; }

  // eslint-disable-next-line class-methods-use-this
  getClassName() { return Conversation.getClassName(); }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = `employee/${this.uid}/leaves/${this.id}/${Conversation.collectionName}`;
    }
    return collectionPath;
  }

  validate() {
    if (!this.uid) {
      throw new Error(
        'uid is required to fetch, create, delete or update note',
      );
    }
    super.validate();
  }

  create() {
    this.sentAt = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    return super.create();
  }

  update() {
    // Set the dateCreated before calling superclass method to create
    this.sentAt = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    return super.update();
  }
}

registerClass(Conversation.getClassName(), Conversation);
