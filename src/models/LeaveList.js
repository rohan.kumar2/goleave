import ListModel from '@/models/ListModel';
import Leave from '@/models/Leave';

export default class LeaveList extends ListModel {
  constructor(args) {
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }
    if (args && args.noteid) {
      this.noteid = args.noteid;
    }
  }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = `employee/${this.noteid}/${Leave.collectionName}`;
    }
    return collectionPath;
  }

  create(doc) {
    console.log(`Creating new Leave with id: ${doc.id} from firebase doc`);
    return new Leave({
      uid: this.uid, noteid: this.noteid, id: doc.id, ...doc.data(),
    });
  }
}
