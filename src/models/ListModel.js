import * as firebase from 'firebase/app';
import 'firebase/firestore';

import Logger from '@/logger/index';
import IllegalArgumentError from '@/exceptions/IllegalArgumentError';
import EmptyPageError from '@/exceptions/EmptyPageError';
import InvalidPathError from '@/exceptions/InvalidPathError';
import Page from '@/models/Page';
import { getClassFromName } from '@/utils/globals';

/**
 * ListModel class is the base class to fetch collection data from
 * Firebase. It provides functionality to fetch data with pagination
 * too.
 *
 * To use ListModel class, you need to extend this class for the type
 * of collection you wish to fetch. The subclass needs to be
 * instantitated by the caller component by passing a required parameter
 * `className`. The parameters are passed as object with various
 * properties so that multiple arguments can be passed. The `className`
 * property of constructor argument should be of the Model subclass
 * whose list you want to store in ListModel.
 *
 * Apart from subclassing the ListModel, you also need to implement a
 * Model subclass whose className you need to pass to the constructor
 * of your subclass model.
 *
 * The methods that you need to
 * override / implement in the ListModel subclass are:
 *
 * 1. get path() {} -- This should return the path to the collection.
 * 2. create(doc) {} -- This should create an return an instance of
 *                      the model type you pull in data
 *
 * The methods that it ListMode exposes are:
 * push -- Add a model instance to the internal list
 * get -- Returns the internal list of model objects
 * fetch -- Fetch and return the promise to fetch the collection as list
 *          from firebase
 * next -- Returns the next page of list
 * prev -- Returns the previous page of list
 */
export default class ListModel {
  constructor(args) {
    if (!args.className) {
      Logger.debug(`ListModel::constructor args: ${args}`);
      throw new Error(
        'className is required to create a ListModel of given class',
      );
    }
    console.log('hello this is listmodel class');
    const cls = getClassFromName(args.className);
    if (!cls) {
      throw new IllegalArgumentError(
        `No registered class found with name ${args.className} `,
      );
    } else {
      this.class = cls;
      this.className = args.className;
    }

    this.modelList = [];
    // TODO: Do not hardcode the pageSize
    this.pageSize = 5;
    this.lastVisible = null;
    this.startIndex = 0;
    this.pageNum = 0;
  }

  /**
   * Method to push an item to internally maintained list
   * @param {*} item
   */
  push(item) {
    // TODO: Check if the item being pushed is of the class for
    // this.className
    this.modelList.push(item);
  }

  /**
   * Method to get the internally maintained list
   */
  get() {
    console.log('Returning this.modelList');
    return this.modelList;
  }

  /**
   * Checks if the path is valid else throws InvalidPathError
   */
  checkPath() {
    if (!this.path) {
      throw new InvalidPathError(
        'Cannot fetch object without valid path to collection',
      );
    }
  }

  /**
   * Returns next page by slicing from this.modelList if the page has
   * already been fetched else it fetches it from firebase
   */
  next() {
    let promise = null;
    this.pageNum += 1;
    this.startIndex += this.pageSize;

    if (this.startIndex >= this.modelList.length) {
    //   Logger.debug(`next: ${this.startIndex} >= ${this.modelList.length}.
    //     Fectching more`);
      console.log(`next: ${this.startIndex} >= ${this.modelList.length}.
        Fectching more`);
      promise = this.fetch(
        { filter: this.filter, orderBy: this.orderBy, paginate: true },
      ).then(() => {
        // Logger.debug(`after fetch list length: ${this.modelList.length}`);
        const page = new Page({
          data: this.modelList.slice(this.startIndex,
            this.startIndex + this.pageSize),
          startIndex: this.startIndex,
          pageNum: this.pageNum,
        });
        // Logger.debug(`Sliced list: ${ret.length}`);
        return page;
      }).catch((error) => {
        if (error instanceof EmptyPageError) {
          this.startIndex -= this.pageSize;
          this.pageNum -= 1;
          //   Logger.debug(`next: Got EmptyPageError. Resetting startIndex to
          //     ${this.startIndex}`);
          console.log(`next: Got EmptyPageError. Resetting startIndex to
            ${this.startIndex}`);
          return new Page({
            data: this.modelList.slice(this.startIndex,
              this.startIndex + this.pageSize),
            startIndex: this.startIndex,
            pageNum: this.pageNum,
            lastPage: true,
          });
        }
        throw error;
      });
    } else {
    //   Logger.debug(`next: startIndex: ${this.startIndex},
    //     pageNum: ${this.pageNum}`);
      console.log(`next: startIndex: ${this.startIndex},
        pageNum: ${this.pageNum}`);
      const page = new Page({
        data: this.modelList.slice(this.startIndex,
          this.startIndex + this.pageSize),
        startIndex: this.startIndex,
        pageNum: this.pageNum,
      });
      promise = new Promise((resolve) => {
        resolve(page);
      });
    }
    return promise;
  }

  /**
   * Returns previous page by slicing from this.modelList
   */
  prev() {
    if (this.startIndex !== 0) {
      this.startIndex -= this.pageSize;
      this.pageNum -= 1;
    }
    // Logger.debug(`prev: startIndex: ${this.startIndex}, pageNum: ${this.pageNum}`);
    console.log(`prev: startIndex: ${this.startIndex}, pageNum: ${this.pageNum}`);
    const page = new Page({
      data: this.modelList.slice(this.startIndex,
        this.startIndex + this.pageSize),
      startIndex: this.startIndex,
      pageNum: this.pageNum,
    });

    return new Promise((resolve) => {
      resolve(page);
    });
  }

  /**
   * Fetches collection from Firebase. If `args` has paginate set to
   * true, it returns a Page else complete collection
   * @param {*} args
   */
  fetch(args) {
    // Check for path before making any firebase call
    this.checkPath();

    let ref = firebase.firestore().collection(`${this.path}`);
    console.log('this is path', this.path);
    // firebase.firestore().collection(`${this.path}`)
    //   .get().then((snap) => {
    //     console.log('thisi is actual data ',snap);
    // })
    //   firebase.firestore().collection('leavetype').onSnapshot(res => {
    //       const changes = res.docChanges();
    //       let value = [];
    //       changes.forEach(element => {
    //         if(element.type === 'added'){
    //           value.push(element.doc.data());
    //         }
    //       });
    //       console.log('this is actual data', value);
    //     })
    if (args && args.filter) {
      ref = ref.where(args.filter);
    }

    if (args && args.orderBy) {
      ref = ref.orderBy(args.orderBy);
    }

    if (args && args.paginate) {
      if (this.lastVisible) {
        // Logger.debug(`Start after ${this.lastVisible}`);
        console.log(`Start after ${this.lastVisible}`);
        ref = ref.startAfter(this.lastVisible);
      }
      ref = ref.limit(this.pageSize);
      this.paginate = true;
      this.filter = args.filter;
      this.orderBy = args.orderBy;
    } else if (args && args.limit) {
      ref = ref.limit(args.limit);
    }
    ref.get().then((snap) => {
      console.log('thisi is actual data ', snap);
    });
    return ref
      .get()
      .then((snapShot) => {
        if (args && args.paginate) {
          // Logger.debug(`snapShot docs: ${snapShot.docs.length}`);
          console.log(`snapShot docs: ${snapShot.docs.length}`);
          if (snapShot.docs.length > 0) {
            this.lastVisible = snapShot.docs[snapShot.docs.length - 1];
          } else {
            throw new EmptyPageError('No more data');
          }
        }
        snapShot.forEach((doc) => {
          console.log('these all ID', doc);
        });
        snapShot.forEach((doc) => {
          // Logger.debug(`Got ${this.className} with id ${doc.id}`);
          console.log(`Got ${this.className} with id ${doc.id}`);
          // Create an object using factory method `create` of this.class
          // and push it to the list. Since the ListModel cannot know
          // how to actually create a model object to be inserted to
          // this.modelList, the child class is expected to override
          // create method which returns an object of the type of the
          // child class after creating the object
          this.push(
            this.create(doc),
          );
        });

        if (this.paginate) {
          // Logger.debug('Returning page');
          console.log('Returning page');
          return new Page({
            data: this.get().slice(this.startIndex,
              this.startIndex + this.pageSize),
            pageNum: this.pageNum,
            startIndex: this.startIndex,
          });
        }
        // Logger.debug('Returning complete list');
        console.log('Returning complete list');
        return this.get();
      })
      .catch((error) => {
        if (!(error instanceof EmptyPageError)) {
          console.log('Could not get notes', error);
          // Logger.error('Could not get notes', error);
        }
        throw error;
      })
      .finally(() => {});
  }

  fetch2(args) {
    // Check for path before making any firebase call
    this.checkPath();

    let ref = firebase.firestore().collection(`${this.path}`);
    console.log('this is path', this.path);
    // firebase.firestore().collection(`${this.path}`)
    //   .get().then((snap) => {
    //     console.log('thisi is actual data ',snap);
    // })
    //   firebase.firestore().collection('leavetype').onSnapshot(res => {
    //       const changes = res.docChanges();
    //       let value = [];
    //       changes.forEach(element => {
    //         if(element.type === 'added'){
    //           value.push(element.doc.data());
    //         }
    //       });
    //       console.log('this is actual data', value);
    //     }
    if (args && args.filter) {
      ref = ref.where(args.filter);
    }

    if (args && args.orderBy) {
      ref = ref.orderBy(args.orderBy);
    }
    // if (args && args.paginate) {
    //   if (this.lastVisible) {
    //     // Logger.debug(`Start after ${this.lastVisible}`);
    //     console.log(`Start after ${this.lastVisible}`);
    //     ref = ref.startAfter(this.lastVisible);
    //   }
    //   ref = ref.limit(this.pageSize);
    //   this.paginate = true;
    //   this.filter = args.filter;
    //   this.orderBy = args.orderBy;
    // } else if (args && args.limit) {
    //   ref = ref.limit(args.limit);
    // }
    ref.get().then((snap) => {
      console.log('thisi is actual data ', snap);
    });
    return ref
      .get()
      .then((snapShot) => {
        // if (args && args.paginate) {
        //   // Logger.debug(`snapShot docs: ${snapShot.docs.length}`);
        //   console.log(`snapShot docs: ${snapShot.docs.length}`);
        //   if (snapShot.docs.length > 0) {
        //     this.lastVisible = snapShot.docs[snapShot.docs.length - 1];
        //   } else {
        //     throw new EmptyPageError('No more data');
        //   }
        // }
        // snapShot.forEach((doc) => {
        //   console.log('these all ID', doc);
        // });
        snapShot.forEach((doc) => {
          // Logger.debug(`Got ${this.className} with id ${doc.id}`);
          console.log(`Got ${this.className} with id ${doc.id}`);
          // Create an object using factory method `create` of this.class
          // and push it to the list. Since the ListModel cannot know
          // how to actually create a model object to be inserted to
          // this.modelList, the child class is expected to override
          // create method which returns an object of the type of the
          // child class after creating the object
          this.push(
            this.create(doc),
          );
        });

        // if (this.paginate) {
        //   // Logger.debug('Returning page');
        //   console.log('Returning page');
        //   return new Page({
        //     data: this.get().slice(this.startIndex,
        //       this.startIndex + this.pageSize),
        //     pageNum: this.pageNum,
        //     startIndex: this.startIndex,
        //   });
        // }
        // Logger.debug('Returning complete list');
        console.log('Returning complete list');
        return this.get();
      })
      .catch((error) => {
        if (!(error instanceof EmptyPageError)) {
          console.log('Could not get notes', error);
          // Logger.error('Could not get notes', error);
        }
        throw error;
      })
      .finally(() => {});
  }
}
