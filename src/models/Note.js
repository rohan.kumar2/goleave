import Vue from 'vue';
import Model from '@/models/Model';
import { registerClass, getClassFromName } from '@/utils/globals';

export default class Note extends Model {
  static collectionName = 'notes';

  static fields = {
    id: {
      type: String,
      default: null,
      required: false,
    },
    leavetype: {
      type: String,
      default: '',
      required: true,
    },
    description: {
      type: String,
      default: '',
      required: true,
    },
    dateCreated: {
      type: Number,
      default: null,
      required: false,
    },
    dateUpdated: {
      type: Number,
      default: null,
      required: false,
    },
  };

  constructor(args) {
    console.log('hello this is note classs');
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }
  }

  static getClassName() { return 'Note'; }

  // eslint-disable-next-line class-methods-use-this
  getClassName() { return Note.getClassName(); }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = 'leavetype';
    }
    return collectionPath;
  }

  validate() {
    if (!this.uid) {
      throw new Error(
        'uid is required to fetch, create, delete or update note',
      );
    }
    super.validate();
  }

  initializeSubCollection(subCollectionField) {
    const cls = getClassFromName(this.getClassName());
    console.log(`this.uid: ${this.uid}, this.id: ${this.id}`);
    this[subCollectionField] = new cls.subCollections[subCollectionField].Type({
      className: cls.subCollections[subCollectionField].subType.getClassName(),
      uid: this.uid,
      noteid: this.id,
    });
  }

  /**
   * Initializes the Subcollection object passed before it can be
   * serialized to server / firebase
   * @param {subType} val
   */

  create() {
    // Set the dateCreated before calling superclass method to create
    this.dateCreated = Vue.moment.utc().format('x');
    return super.create();
  }

  update() {
    this.dateUpdated = Vue.moment.utc().format('x');
    return super.update();
  }
}

// Register class in global map
registerClass(Note.getClassName(), Note);
