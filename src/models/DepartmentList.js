import ListModel from '@/models/ListModel';
import DepartmentNote from '@/models/DepartmentNote';

export default class DepartmentList extends ListModel {
  constructor(args) {
    super(args);
    if (!args.uid) {
      throw new Error('uid is required for fetching Notes for a User');
    }
    console.log('this is department list model');
    this.uid = args.uid;
  }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = 'department';
    }
    return collectionPath;
  }

  create(doc) {
    console.log(`Creating new note with id: ${doc.id} from firebase doc`);
    return new DepartmentNote({ uid: this.uid, id: doc.id, ...doc.data() });
  }
}
