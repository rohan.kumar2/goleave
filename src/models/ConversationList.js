import ListModel from '@/models/ListModel';
import Conversation from '@/models/Conversation';
// import ApplyLeave from '@/models/ApplyLeave';

export default class ConversationList extends ListModel {
  static collectionName= 'conversation';

  constructor(args) {
    super(args);
    if (!args.uid) {
      throw new Error('uid is required for fetching Notes for a User');
    }
    console.log('this is department list model');
    this.uid = args.uid;
    this.id = args.id;
  }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = `employee/${this.uid}/leaves/${this.id}/${Conversation.collectionName}`;
    }
    return collectionPath;
  }

  create(doc) {
    console.log(`Creating new note with id: ${doc.id} from firebase doc`);
    return new Conversation({ uid: this.uid, id: doc.id, ...doc.data() });
  }
}
