import ListModel from '@/models/ListModel';
import ApplyLeave from '@/models/ApplyLeave';

export default class ApplyLeaveList extends ListModel {
  static collectionName= 'leaves';

  constructor(args) {
    super(args);
    if (!args.uid) {
      throw new Error('uid is required for fetching Notes for a User');
    }
    console.log('this is department list model');
    this.uid = args.uid;
  }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = `employee/${this.uid}/${ApplyLeave.collectionName}`;
    }
    return collectionPath;
  }

  create(doc) {
    console.log(`Creating new note with id: ${doc.id} from firebase doc`);
    return new ApplyLeave({ uid: this.uid, id: doc.id, ...doc.data() });
  }
}
