import Vue from 'vue';
import Model from '@/models/Model';
import { registerClass, getClassFromName } from '@/utils/globals';
import Leave from '@/models/Leave';
import LeaveList from '@/models/LeaveList';

export default class Employee extends Model {
  static collectionName= 'employee';

  static fields = {
    id: {
      type: String,
      default: null,
      required: false,
    },
    employeecode: {
      type: String,
      default: '',
      required: true,
    },
    gender: {
      type: String,
      default: '',
      required: true,
    },
    department: {
      type: String,
      default: '',
      required: true,
    },
    DOB: {
      type: String,
      default: '',
      required: true,
    },
    firstname: {
      type: String,
      default: '',
      required: true,
    },
    lastname: {
      type: String,
      default: '',
      required: true,
    },
    address: {
      type: String,
      default: '',
      required: true,
    },
    city: {
      type: String,
      default: '',
      required: true,
    },
    country: {
      type: String,
      default: '',
      required: true,
    },
    mobilenumber: {
      type: String,
      default: '',
      required: true,
    },
    email: {
      type: String,
      default: '',
      required: true,
    },
    dateCreated: {
      type: String,
      default: null,
      required: false,
    },
    dateUpdated: {
      type: String,
      default: null,
      required: false,
    },
  };

  static subCollections = {
    Leavefield: {
      type: LeaveList,
      subType: Leave,
      default: [],
    },
  };

  constructor(args) {
    console.log('hello this is employee note list');
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }
  }

  static getClassName() { return 'Employee'; }

  // eslint-disable-next-line class-methods-use-this
  getClassName() { return Employee.getClassName(); }

  get docPath() {
    return this.uid;
  }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = 'employee';
    }
    return collectionPath;
  }

  initializeSubCollection(subCollectionField) {
    const cls = getClassFromName(this.getClassName());
    console.log(`this.uid: ${this.uid}, this.id: ${this.id}`);
    // eslint-disable-next-line new-cap
    this[subCollectionField] = new cls.subCollections[subCollectionField].type({
      className: cls.subCollections[subCollectionField].subType.getClassName(),
      uid: this.uid,
      noteid: this.id,
    });
  }

  /**
   * Initializes the Subcollection object passed before it can be
   * serialized to server / firebase
   * @param {subType} val
   */
  initializeSubcollectionObject(val, subType) {
    // Initialize the subcollection object based on what fields
    // are required to create / update / delete the subcollection
    // object
    if (subType.getClassName() === Leave.getClassName()) {
      // For workExperience both uid and noteId are required to know
      // under which user collection object and under which note
      // collection object the WorkExperience object needs to be saved
      // eslint-disable-next-line no-param-reassign
      val.uid = this.uid;
      // eslint-disable-next-line no-param-reassign
      val.noteid = this.id;
    }
  }

  validate() {
    if (!this.uid) {
      throw new Error(
        'uid is required to fetch, create, delete or update note',
      );
    }
    super.validate();
  }

  create() {
    this.dateCreated = Vue.moment.utc().format('x');
    return super.create();
  }

  update() {
    this.dateUpdated = Vue.moment.utc().format('x');
    return super.update();
  }
}

registerClass(Employee.getClassName(), Employee);
