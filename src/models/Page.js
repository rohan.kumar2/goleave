export default class Page {
  constructor(args) {
    if (args && args.data) {
      this.data = args.data;
    } else {
      this.data = [];
    }

    if (args && args.startIndex !== undefined) {
      this.startIndex = args.startIndex;
    } else {
      this.startIndex = 0;
    }

    if (args && args.pageNum !== undefined) {
      this.pageNum = args.pageNum;
    } else {
      this.pageNum = 0;
    }

    // Pass last page to indicate that this page is last
    if (args && args.lastPage !== undefined) {
      this.lastPage = args.lastPage;
    }
  }

  get() {
    return this.data;
  }

  hasPrevious() {
    let ret = true;
    if (this.startIndex === 0) {
      ret = false;
    }
    return ret;
  }

  hasNext() {
    if (this.lastPage) {
      return false;
    }
    return true;
  }
}
