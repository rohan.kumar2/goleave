import Vue from 'vue';
import Model from '@/models/Model';
import { registerClass } from '@/utils/globals';

export default class AddRoles extends Model {
  static fields = {
    hr: {
      type: Object,
      default: {
        leaves: ['create', 'update', 'read'],
        user: ['create', 'update', 'read'],
      },
      required: false,
    },
    admin: {
      type: Object,
      default: {
        leaves: ['create', 'update', 'read'],
        user: ['create', 'update', 'read', 'delete'],
      },
      required: false,
    },
    // // photoURL: {
    // //   type: String,
    // //   default:null,
    // //   required: false,
    // // },
    // email: {
    //   type: String,
    //   default: '',
    //   required: true,
    // },
    // password: {
    //   type: String,
    //   default: '',
    //   required: true,
    // },
    // emailVerified: {
    //   type: Boolean,
    //   default: false,
    //   required: true,
    // },
    // phoneNumber: {
    //   type: Number,
    //   default: 0,
    //   required: false,
    // },
    // providerId: {
    //   type: String,
    //   default: null,
    //   required: false,
    // },
    // dateCreated: {
    //   type: Number,
    //   default: null,
    //   required: true,
    // },
    // dateUpdated: {
    //   type: Number,
    //   default: null,
    //   required: false,
    // },
  };

  // static subCollections = {
  //   workExperienceList: {
  //     type: AddRolesWorkExperienceList,
  //     subType: WorkExperience,
  //     default: [],
  //   },
  // };

  constructor(args) {
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }
  }

  static getClassName() { return 'AddRoles'; }

  // eslint-disable-next-line class-methods-use-this
  getClassName() { return AddRoles.getClassName(); }

  // eslint-disable-next-line class-methods-use-this
  get path() {
    return '/roles/';
  }

  validate() {
    super.validate();
  }

  // initializeSubCollection(subCollectionField) {
  //   const cls = getClassFromName(this.getClassName());
  //   console.log(`this.uid: ${this.uid}, this.id: ${this.id}`);
  //   this[subCollectionField] = new cls.subCollections[subCollectionField].type({
  //     className: cls.subCollections[subCollectionField].subType.getClassName(),
  //     uid: this.uid,
  //     noteid: this.id,
  //   });
  // }

  /**
   * Initializes the Subcollection object passed before it can be
   * serialized to server / firebase
   * @param {subType} val
   */
  // initializeSubcollectionObject(val, subType) {
  //   // Initialize the subcollection object based on what fields
  //   // are required to create / update / delete the subcollection
  //   // object
  //   if (subType.getClassName() === WorkExperience.getClassName()) {
  //     // For workExperience both uid and noteId are required to know
  //     // under which AddRoles collection object and under which note
  //     // collection object the WorkExperience object needs to be saved
  //     // eslint-disable-next-line no-param-reassign
  //     val.uid = this.uid;
  //     // eslint-disable-next-line no-param-reassign
  //     val.noteid = this.id;
  //   }
  // }

  getJSONObject() {
    // Set the dateCreated before calling superclass method to create
    // this.dateCreated = Vue.moment.utc().format('x');
    return super.getJSONObject();
  }

  update() {
    this.dateUpdated = Vue.moment.utc().format('x');
    return super.update();
  }
}

// Register class in global map
registerClass(AddRoles.getClassName(), AddRoles);
