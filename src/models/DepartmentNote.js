import Vue from 'vue';
import Model from '@/models/Model';
import { registerClass } from '@/utils/globals';

export default class DepartmentNote extends Model {
  static collectionName= 'departmentnotes';

  static fields = {
    id: {
      type: String,
      default: null,
      required: false,
    },
    departmentname: {
      type: String,
      default: '',
      required: true,
    },
    departmentcode: {
      type: String,
      default: '',
      required: true,
    },
    departmentshortname: {
      type: String,
      default: '',
      required: true,
    },
    dateCreated: {
      type: String,
      default: null,
      required: false,
    },
    dateUpdated: {
      type: String,
      default: null,
      required: false,
    },
  };

  constructor(args) {
    console.log('hello this is department note list');
    super(args);
    if (args && args.uid) {
      this.uid = args.uid;
    }
  }

  static getClassName() { return 'DepartmentNote'; }

  // eslint-disable-next-line class-methods-use-this
  getClassName() { return DepartmentNote.getClassName(); }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = 'department';
    }
    return collectionPath;
  }

  validate() {
    if (!this.uid) {
      throw new Error(
        'uid is required to fetch, create, delete or update note',
      );
    }
    super.validate();
  }

  create() {
    this.dateCreated = Vue.moment.utc().format('x');
    return super.create();
  }

  update() {
    // Set the dateCreated before calling superclass method to create
    this.dateCreated = Vue.moment.utc().format('x');
    return super.update();
  }
}

registerClass(DepartmentNote.getClassName(), DepartmentNote);
