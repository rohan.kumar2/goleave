import ListModel from '@/models/ListModel';
import Employee from '@/models/Employee';

export default class EmployeeList extends ListModel {
  constructor(args) {
    super(args);
    if (!args.uid) {
      throw new Error('uid is required for fetching Notes for a User');
    }
    console.log('this is Employee list model');
    this.uid = args.uid;
  }

  get path() {
    let collectionPath = null;
    if (this.uid) {
      collectionPath = 'employee';
    }
    return collectionPath;
  }

  create(doc) {
    console.log(`Creating new note with id: ${doc.id} from firebase doc`);
    return new Employee({ uid: this.uid, id: doc.id, ...doc.data() });
  }
}
