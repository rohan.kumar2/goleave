import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store/index';
import Home from '@/views/Home.vue';
import LeaveView from '@/views/LeaveView.vue';

// SignIn Section
import Signin from '@/views/login/Signin.vue';

// Employee information section
import AddEmployee from '@/views/employee/AddEmployee.vue';
import ListEmployee from '@/views/employee/ListEmployee.vue';
import EmployeeEdit from '@/views/employee/EmployeeEdit.vue';

// Department Detail Section
import DepartmentForm from '@/views/department/AddDepartment.vue';
import DepartmentList from '@/views/department/ListDepartment.vue';
import DepartmentEdit from '@/views/department/DepartmentEdit.vue';

// Leave information Section
import ListLeave from '@/views/leave/ListLeave.vue';
import LeaveForm from '@/views/leave/AddLeave.vue';
import LeaveEdit from '@/views/leave/LeaveEdit.vue';

// Employee panel section
import EmplLeave from '@/views/emplPanel/EmplLeave.vue';
import ChangePass from '@/views/emplPanel/ChangePass.vue';
import EmployeeInfo from '@/views/emplPanel/EmployeeInfo.vue';
import ApplyLeave from '@/views/emplPanel/ApplyLeave.vue';
import EditLeave from '@/views/emplPanel/EditLeave.vue';
import Conversation from '@/views/emplPanel/Conversation.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
  },
  {
    path: '/addemployee',
    name: 'AddEmployee',
    component: AddEmployee,
  },
  {
    path: '/employeeEdit/:id',
    name: 'EmployeeEdit',
    component: EmployeeEdit,
    props: true,
  },
  {
    path: '/listemployee',
    name: 'ListEmployee',
    component: ListEmployee,
  },
  {
    path: '/leavelist',
    name: 'LeaveList',
    component: ListLeave,
  },
  {
    path: '/leavelistedit/:id',
    name: 'LeaveEdit',
    component: LeaveEdit,
    props: true,
  },
  {
    path: '/leaveform',
    name: 'LeaveForm',
    component: LeaveForm,
  },
  {
    path: '/departmentedit/:id',
    name: 'DepartmentEdit',
    component: DepartmentEdit,
    props: true,
  },
  {
    path: '/departmentform',
    name: 'DepartmentForm',
    component: DepartmentForm,
  },
  {
    path: '/departmentlist',
    name: 'DepartmentList',
    component: DepartmentList,
  },
  {
    path: '/signin',
    name: 'Signin',
    component: Signin,
    meta: { requiresAuth: false },
  },
  {
    path: '/employeeleave',
    name: 'Employeeleave',
    component: EmplLeave,
  },
  {
    path: '/changepassword',
    name: 'ChangePass',
    component: ChangePass,
  },
  {
    path: '/employeeinformation',
    name: 'EmployeeInfo',
    component: EmployeeInfo,
  },
  {
    path: '/applyleave',
    name: 'ApplyLeave',
    component: ApplyLeave,
  },
  {
    path: '/editleave',
    name: 'EditLeave',
    component: EditLeave,
    props: true,
  },
  {
    path: '/leaveview/:uid/:id',
    name: 'LeaveView',
    component: LeaveView,
    props: true,
  },
  {
    path: '/conversation',
    name: 'Conversation',
    component: Conversation,
    props: true,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  console.log(`Router navigating ${from.path} -> ${to.path}`,
    to.matched.length);
  const doesNotRequireAuth = to.matched.some(
    (record) => (record.meta && record.meta.requiresAuth === false),
  );

  // If user is navigated to signin, OR
  // If the page does not require authentication, continue
  if (to.name === 'Signin' || doesNotRequireAuth) {
    next();
  } else if (to.matched.length === 0) {
    // If there no matching route, navigate to /signin
    // or possibly show a 404 page
    next({ name: 'Signin' });
  } else if (!store.state.user) {
    // If there is no user state in store, then redirect to /signin
    console.log(`Route ${to.name} requires authentication. Setting redirectToUrl`);
    console.log(`store.state.user ${store.state.user}`);
    // Set the route 'to' navigate in store so that the user
    // can be redirected back to it after authentication
    store.dispatch('setRedirectRoute', to);
    next({ name: 'Signin' });
  } else {
    // If to route is not /signin and requiresAuth and user is also set
    // then continue
    console.log('Route requiresAuth but user is present in store...continuing');
    next();
  }
});

export default router;
