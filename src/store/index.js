import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    drawer: null,
    user: null,
    userid: null,
    accountcreated: false,
    redirectRoute: null,
    overlay: false,
    snackbarText: '',
    showSnackbar: false,
    menuList: [],
    temp: true,
    employeeMenu: [
      {
        title: 'profile info',
        icon: 'mdi-face',
        to: '/employeeinformation',
      },
      {
        title: 'leave list',
        icon: 'mdi-clipboard-text',
        to: 'EmployeeLeave',
      },
      {
        title: 'change password',
        icon: 'mdi-lock-open-outline',
        to: '/changepassword',
      },
    ],
    adminMenu: [
      {
        icon: 'mdi-view-dashboard',
        title: 'dashboard',
        to: '/home',
      },
      {
        icon: 'mdi-account',
        title: 'employee',
        to: '/listemployee',
      },
      {
        title: 'department',
        icon: 'mdi-home',
        to: '/departmentlist',
      },
      {
        title: 'leave type',
        icon: 'mdi-calendar-plus',
        to: '/leavelist',
      },
      {
        title: 'add salary',
        icon: 'mdi-cash',
        to: '/home',
      },
    ],
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getUserid(state) {
      return state.userid;
    },
    getAccountCreated(state) {
      return state.accountcreated;
    },
    getRedirectRoute(state) {
      return state.redirectRoute;
    },
    getOverlay(state) {
      return state.overlay;
    },
    getSnackbarText(state) {
      return state.snackbarText;
    },
    getShowSnackbar(state) {
      return state.showSnackbar;
    },
    getNavigationList(state) {
      return state.menuList;
    },
  },
  mutations: {
    SET_USER(state, user) {
      state.user = user;
    },
    SET_USERID(state, userid) {
      state.userid = userid;
    },
    SET_ACCOUNT_CREATED(state, accountcreated) {
      state.accountcreated = accountcreated;
    },
    SET_USER_PROFILE(state, profile) {
      state.user.userprofile = profile;
    },
    SET_REDIRECT_ROUTE(state, route) {
      state.redirectRoute = route;
    },
    SET_OVERLAY(state, value) {
      state.overlay = value;
    },
    SET_SNACKBAR_TEXT(state, value) {
      state.snackbarText = value;
    },
    SET_SHOW_SNACKBAR(state, value) {
      state.showSnackbar = value;
    },
    SET_BAR_IMAGE(state, payload) {
      state.barImage = payload;
    },
    SET_DRAWER(state, payload) {
      state.drawer = payload;
    },
    SET_NAVIGATION_LIST(state, payload) {
      state.menuList = payload;
    },
  },
  actions: {
    setUser({ commit }, user) {
      commit('SET_USER', user);
    },
    setUserid({ commit }, userid) {
      commit('SET_USER', userid);
    },
    setAcountCreated({ commit }, accountcreated) {
      commit('SET_ACCOUNT_CREATED', accountcreated);
    },
    setUserProfile({ commit }, profile) {
      commit('SET_USER_PROFILE', profile);
    },
    setRedirectRoute({ commit }, route) {
      commit('SET_REDIRECT_ROUTE', route);
    },
    setOverlay({ commit }, value) {
      commit('SET_OVERLAY', value);
    },
    setSnackbarText({ commit }, value) {
      commit('SET_SNACKBAR_TEXT', value);
    },
    setShowSnackbar({ commit }, value) {
      commit('SET_SHOW_SNACKBAR', value);
    },
    setNavigationList({ commit }, value) {
      commit('SET_NAVIGATION_LIST', value);
    },
  },
  modules: {
  },
});
