import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import colors from 'vuetify/lib/util/colors';

Vue.use(Vuetify);

const vuetify = new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#1b0052',
        secoundary: '#e10067',
        success: colors.green,
        accent: '#1976D2',
        warning: colors.orange,
        info: colors.yellow,
        error: colors.red,
      },
    },
  },
});

export default vuetify;
