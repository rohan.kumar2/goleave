import Vue from 'vue';

export default class Logger {
  static debug(...args) {
    Vue.$log.debug(...args);
  }

  static info(...args) {
    Vue.$log.info(...args);
  }

  static warn(...args) {
    Vue.$log.warn(...args);
  }

  static error(...args) {
    Vue.$log.error(...args);
  }
}
