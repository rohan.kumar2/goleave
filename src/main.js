import Vue from 'vue';
import VueLogger from 'vuejs-logger';

import initFirebase from '@/firebase/index';
import VueMoment from 'vue-moment';

import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;

function initVue() {
  // Use moment for date
  Vue.use(VueMoment);

  new Vue({
    router,
    store,
    vuetify,
    render: (h) => h(App),
    created() {
      initFirebase(this);
    },
  }).$mount('#app');
}

function init() {
  // configure logger
  const isProduction = process.env.NODE_ENV === 'production';
  const options = {
    isEnabled: true,
    logLevel: isProduction ? 'error' : 'debug',
    stringifyArguments: false,
    showLogLevel: true,
    showMethodName: true,
    separator: '|',
    showConsoleColors: true,
  };
  Vue.use(VueLogger, options);
  initVue();
}

init();
