import * as firebase from 'firebase/app';
import 'firebase/firestore';

import Logger from '@/logger/index';
import store from '@/store/index';
import router from '@/router';

function signOut() {
  store.dispatch('setOverlay', true);
  firebase.auth().signOut().then(() => {
    store.dispatch('setOverlay', false);
    store.dispatch('setSnackbarText', 'Logout successfully');
    store.dispatch('setShowSnackbar', true);
    router.push('/signin');
    store.dispatch('setUser', null);
    setTimeout(() => {
      store.dispatch('setShowSnackbar', false);
    }, 4000);
  }).catch((error) => {
    console.log(error);
    store.dispatch('setSnackbarText', error.message);
    store.dispatch('setShowSnackbar', true);
    store.dispatch('setOverlay', false);
    setTimeout(() => {
      store.dispatch('setShowSnackbar', false);
    }, 4000);
  });
}

/* This is fetching size of all department in leave management */

async function getDepartment() {
  return firebase.firestore().collection('department').get()
    .then((snap) => snap.size)
    .catch((error) => {
      console.log('this is error', error);
    });
}
async function getDepartmentList() {
  return firebase.firestore().collection('department').get()
    .then((snap) => {
      const array = [];
      snap.forEach((doc) => {
        array.push(doc.data().departmentname);
      });
      return array;
    })
    .catch((error) => {
      console.log('this is error', error);
    });
}
async function getLeaveTypeList() {
  return firebase.firestore().collection('leavetype').get()
    .then((snap) => {
      const array = [];
      snap.forEach((doc) => {
        array.push(doc.data().leavetype);
      });
      return array;
    })
    .catch((error) => {
      console.log('this is error', error);
    });
}

async function getLeaveType() {
  return firebase.firestore().collection('leavetype').get()
    .then((snap) => snap.size)
    .catch((error) => {
      console.log('this is error', error);
    });
}

async function getUserSize() {
  return firebase.firestore().collection('employee').get()
    .then((snap) => snap.size)
    .catch((error) => {
      console.log('this is error', error);
    });
}

/* END OF ALL FETCHING DETAILS METHODS */

function getCurrentUser() {
  return firebase.auth().currentUser;
}

function getProfile() {
  const { uid } = getCurrentUser();
  return firebase.firestore()
    .collectiongetAuthenticatedUser('user')
    .doc(uid)
    .get()
    .then((doc) => {
      console.log(doc.id, '=>', doc.data());
      return { id: doc.id, ...doc.data() };
    })
    .catch((error) => {
      console.log(error);
      Logger.error(
        `Could not get user with id: ${uid}`,
      );
      Promise.reject(error);
    })
    .finally(() => {
      store.dispatch('setOverlay', false);
    });
}

function setProfile(profile) {
  const { uid } = getCurrentUser();
  return firebase.firestore()
    .collection('users')
    .doc(uid)
    .set(profile)
    .then((docRef) => {
      console.log(docRef);
      // Logger.info(`Created profile for user with id: ${uid}`);
      console.log(`Created profile for user with id: ${uid}`);
    })
    .catch((error) => {
      console.log(error);
      // Logger.error(`Could not create profile ${JSON.stringify(profile, null, 2)}`);
      console.log(`Could not create profile ${JSON.stringify(profile, null, 2)}`);
    })
    .finally(() => {
      store.dispatch('setOverlay', false);
    });
}

function sendEmailVerification() {
  const actionCodeSettings = {
    url: 'https://localhost:8080/?email=user@example.com&cartId=123',
    handleCodeInApp: true,
  };

  firebase.auth()
    .currentUser
    .sendEmailVerification(actionCodeSettings)
    .then(() => {
      Logger.info('Sent Verification Email');
    })
    .catch((error) => {
      Logger.error(`Could not send verification email: ${error.code}, ${error.message}`);
    });
}

export {
  signOut,
  getCurrentUser,
  getDepartment,
  getDepartmentList,
  getLeaveTypeList,
  getUserSize,
  getLeaveType,
  getProfile,
  setProfile,
  sendEmailVerification,
};
