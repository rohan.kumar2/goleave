import * as firebase from 'firebase';
import 'firebase/firestore';

import Logger from '@/logger/index';

let db = null;

function onAuthStateChanged(app, user) {
  // console.log(`onAuthStateChanged: ${this}`);
  if (user) {
    // console.log('onAuthStateChanged got user');
    // console.log(`onAuthStateChanged ${JSON.stringify(user, null, 2)}`);
    // console.log(`state: ${JSON.stringify(app.$store.state, null, 2)}`);

    // Set the state in store if it is not already set
    if (!app.$store.state.user) {
      app.$store.dispatch('setUser', user);
    }

    // check if there is a redirectToUrl set and redirect there
    const { redirectRoute } = app.$store.state;
    if (redirectRoute) {
      // console.log(`redirectToUrl is not NULL ${JSON.stringify(redirectRoute, null, 2)}`);
      // console.log(`currentRoute: ${app.$router.currentRoute.name}`);
      // Before redirecting to `redirectUrl` set it to null in store
      app.$store.dispatch('setRedirectRoute', null);
      firebase.auth().currentUser.getIdTokenResult()
        .then((idTokenResult) => {
          // Confirm the user is an Admin.
          if (idTokenResult.claims.admin || idTokenResult.claims.manager) {
            app.$router.push({ name: redirectRoute.name, params: redirectRoute.params });
          } else {
            app.$router.push({ name: 'EmployeeInfo' });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    } else if (app.$router.currentRoute.name === 'Signin') {
      // Redirect the user to home route if the user is present
      firebase.auth().currentUser.getIdTokenResult()
        .then((idTokenResult) => {
          // Confirm the user is an Admin.
          if (idTokenResult.claims.admin || idTokenResult.claims.manager) {
            // Show admin UI.
            app.$router.push({ name: 'home' });
          } else {
            // Show regular user UI.
            app.$router.push({ name: 'EmployeeInfo' });
          }
        })
        .catch((error) => {
          console.log(error);
        });
      // app.$router.push({ name: 'home' });
    }
    //  else if (app.$router.currentRoute.name !== 'home') {
    //   // Else redirect to default location, if the user is
    //   // not already on the same route
    //   console.log(`currentRouteName is not home ${app.$router.currentRoute.name}`);
    //   app.$router.push({ name: 'home' });
    // }
  } else {
    console.log('onAuthStateChanged got no user');
    if (app.$router.currentRoute.name !== 'Signin') {
      app.$router.push({ name: 'Signin' });
    }
  }
}

export default function initFirebase(app) {
  const firebaseConfig = {
    apiKey: 'AIzaSyB5D9QoiO0kg5mcQgr9U3tWh63CpSy8cFI',
    authDomain: 'goleave-1a818.firebaseapp.com',
    databaseURL: 'https://goleave-1a818.firebaseio.com',
    projectId: 'goleave-1a818',
    storageBucket: 'goleave-1a818.appspot.com',
    messagingSenderId: '504721565034',
    appId: '1:504721565034:web:f6e20d7254dc068520b113',
    measurementId: 'G-HSWQHH71NC',
  };

  firebase.initializeApp(firebaseConfig);
  // To test cloud function locally using firebase emulators
  firebase.functions().useFunctionsEmulator('http://localhost:5001');

  firebase.auth().onAuthStateChanged((user) => onAuthStateChanged(app, user));
  db = firebase.firestore();

  // If development mode and local emulators running is enabled
  // then change the FIRESTORE_PORT
  if (process.env.NODE_ENV === 'development'
      && process.env.VUE_APP_RUN_LOCAL_EMULATORS === '1') {
    Logger.debug(`Process.env ${JSON.stringify(process.env, null, 2)})`);
    db.settings({
      host: `localhost:${process.env.VUE_APP_EMULATOR_FIRESTORE_PORT}`,
      ssl: false,
    });
  }
}

/**
 * This method waits till a user is resolved so that uid is
 * present before any firebase call is done for the loggedin
 * user
 */
function getAuthenticatedUser() {
  return new Promise((resolve, reject) => {
    firebase.auth().onAuthStateChanged((user) => {
      resolve(user);
    }, reject);
  });
}

function getFirestoreRef() {
  if (db === null) {
    throw Error('Firebase Firestore has not been initialized yet');
  }
  return db;
}

export {
  getAuthenticatedUser,
  getFirestoreRef,
};
