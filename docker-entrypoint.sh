#!/usr/bin/dumb-init /bin/sh

npm install
cd functions && npm install && cd ..
if [ "${VUE_APP_RUN_LOCAL_EMULATORS}" = "1" ]; then
    npx firebase emulators:start &
fi
npm run serve -- --port=${VUE_CLI_SERVICE_PORT}
