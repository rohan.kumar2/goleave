# goleave

Vuejs based project to manage employee leaves.

## Development Setup

* Install Docker v 19.03.8 or above
* Install docker-compose v 1.25.03 or above
* Change .env file to have the same id and EUID as shown by running `id`
  command on your system:
  ```
  $id
  uid=1000(username) gid=1000(username) groups=1000(username), 999(docker)
  ```
* Run ./bin/build-and-run.sh
* This will start firebase emulators too, which you can use to test your code
  in your local machiene.
* Set the value of `VUE_APP_RUN_LOCAL_EMULATORS` in .env file to anything other
  than 1 to disable running of firebase emulators
* Once it builds you should see the container running

```
$docker ps

CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                                                                                                                NAMES
58b43d21f90d        goleave_web         "/app/docker-entrypo…"   10 hours ago        Up 20 minutes       0.0.0.0:5000-5001->5000-5001/tcp, 0.0.0.0:8080-8081->8080-8081/tcp, 0.0.0.0:9005->9005/tcp, 0.0.0.0:9090->9090/tcp   goleave-backend
```
* For logs, run
  docker logs -f goleave-backend

* To stop the docker container, run
  ./bin/stop-dev.sh

* To run any npm command, exec into container using:
```
docker exec -it gohire-backend /bin/sh
```
